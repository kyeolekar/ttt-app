var express = require('express');
const request = require('request');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.post('/', function(req, res, next){
  request('http://terriblytinytales.com/test.txt', function(error, response, body){
    if (response.statusCode === 200) {
      const textFile = body.split(/\n|\t|\s|,/);
      sortedList  = getCountOfWords(textFile)
      // Sort keys based on values -> https://stackoverflow.com/questions/1069666/sorting-javascript-object-by-property-value
      sortedArray = Object.keys(sortedList).sort(function(a,b){return sortedList[b]-sortedList[a]});
      let n = req.body.numberInput;
      userDataArr = []
      for(let i=0; i<n; i++){
        singleton = {}
        singleton['letter'] = sortedArray[i];
        singleton['value'] = sortedList[sortedArray[i]]
        userDataArr.push(singleton);
      }
      console.log(userDataArr)
      res.render('userdata', { data: userDataArr, title: 'Data Table' });
    }
    else {
      res.render('userdata', { title: 'Error communicating with server.', data: {} });
    }
  })
})

function getCountOfWords(words){
  var count = { };
  var pattern = /^[a-zA-Z]+$/;
  words.forEach(function(word){
    if (pattern.test(word)){
      word = word.toLowerCase();
      count[word] = (count[word] || 0) + 1;
    }
  });
  return count;
}

module.exports = router;
