# Live Demo
https://fierce-taiga-72144.herokuapp.com/

# Working
The small app is created using Express js.

Initially the user opens the webpage url, the url is handled by the nodejs backend. The user can then submit the form with one input field which stores the number of words to display.

The post request then sends get request to url http://terriblytinytales.com/test.txt and checks the status code.

The data is then split into a string. The string is then converted into an object which stores the word and it's Frequency. The object is then converted to an sorted array. The data is then converted to be displayed on the frontend.
Lastly, the render method sends the data to frontend.


# Libraries and plugins used.
- Express Js
- Request

# Test cases
Case : Url is down
Input : N/A
Output : Error message stating "Server is down".

Case : Numberic data
Input : 5
Output : Words and Frequency in tabular format.

# Test Data
| i   	| 27 	|
|-----	|----	|
| a   	| 24 	|
| to  	| 23 	|
| you 	| 19 	|
| can 	| 18 	|

# Screenshots
![image1](https://image.prntscr.com/image/R1-oD3fkQEaIrOjuyzhZwA.png "Landing page")
![image2](https://image.prntscr.com/image/jM1AOxQmSxmc5F-hUKrXXg.png "Data page")
